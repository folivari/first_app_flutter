import 'package:flutter/material.dart';

class SlideRouteBuilder extends PageRouteBuilder {
  final Widget page;
  final String direction;

  SlideRouteBuilder({this.page, this.direction})
    : super(pageBuilder: (BuildContext context, Animation<double> animation, 
    Animation<double> secondaryAnimation) {
      return page;
    },
    transitionsBuilder: (BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation, Widget child) {
      var begin;
      switch (direction) {
        case 'vertical':
          begin = Offset(0.0, 1.0);
          break;
        default:
          begin = Offset(1.0, 0.0);
          break;
      }
      var end = Offset.zero;
      var tween = Tween<Offset>(begin: begin, end: end);
      var curvedAnimation = CurvedAnimation(
        parent: animation,
        curve: Curves.ease,
      );

      return SlideTransition(
        position: tween.animate(curvedAnimation),
        child: child,
      );
    }
  );
}
