import 'package:first_app/src/views/components/home_list_item.dart';
import 'package:flutter/material.dart';

class HomePageScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text('Ma première application Flutter'),
      ),
      body: ListView(children: <HomeListItem>[
        HomeListItem(
            title: 'Mentions Légales',
            subtitle:
                'Accéder aux mentions légales (repris du site internet sosh.fr)',
            destination: '/mentions-legales',
            icon: Icon(Icons.support_agent)),
        HomeListItem(
            title: 'Amazon',
            subtitle:
                'Permet de voir un ensemble de produits disponible sur le site Amazon.fr',
            destination: '/amazon',
            icon: Icon(Icons.shopping_cart)),
        HomeListItem(
            title: 'Animation Flutter',
            subtitle:
                'Page permettant de lancer une succession d\'animation avec le package Flutter',
            destination: '/animation',
            icon: Icon(Icons.auto_fix_high)),
        HomeListItem(
            title: 'WishList',
            subtitle:
                'Intégration de la WishList Getting Started de Flutter avec le State Management Provider',
            destination: '/wishlist',
            icon: Icon(Icons.card_giftcard)),
      ]),
    );
  }
}
