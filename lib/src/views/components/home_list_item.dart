import 'package:flutter/material.dart';

class HomeListItem extends StatelessWidget {
  HomeListItem(
      {Key key, this.icon, this.title, this.subtitle, this.destination})
      : super(key: key);

  final Widget icon;
  final String title;
  final String subtitle;
  final String destination;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.of(context).pushNamed(this.destination);
      },
      child: Container(
          margin: EdgeInsets.all(10.0),
          alignment: Alignment.centerLeft,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Padding(
                padding: EdgeInsets.only(right: 20.0),
                child: this.icon,
              ),
              Expanded(
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(this.title,
                          style: TextStyle(
                            fontWeight: FontWeight.w500,
                            fontSize: 18.0,
                          ),
                          softWrap: true,
                          textAlign: TextAlign.left),
                      Text(this.subtitle,
                          style: TextStyle(fontSize: 16.0),
                          softWrap: true,
                          textAlign: TextAlign.left)
                    ]),
              ),
              Icon(Icons.arrow_forward_ios),
            ],
          )),
    );
  }
}
